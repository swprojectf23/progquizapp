# ProgQuizApp

## Name
CodeCrunch

## Description
An application for mobile devices about programming. Test your knowledge in different programming languages with fun quizzes and competitive gamemodes!

## Installation
Within a particular ecosystem, there may be a common way of installing things, such as using Yarn, NuGet, or Homebrew. However, consider the possibility that whoever is reading your README is a novice and would like more guidance. Listing specific steps helps remove ambiguity and gets people to using your project as quickly as possible. If it only runs in a specific context like a particular programming language version or operating system or has dependencies that have to be installed manually, also add a Requirements subsection.

## Usage
You can choose to go through the levels in order in level select or experience one of the online competitive gamemodes. For each question, you will be presented with a codebite, a question and some answer options. Choose the answer you think is correct. As long as you have hearts to spare, it is no problem if you choose the incorrect answer. An explanation of the question will be given to you when you answer correctly.

## Authors
Made by  
Abinav Reddy Aleti [s224786]  
Marius Ditlev Anthony [s224767]  
Martin Handest [s224755]  
Oskar William Ulrich Holland [s224768]  
William Allerup Carlsen [s214881]  
Yahya Alwan [s224739]  
  
For the Software Project course (02128)  

## Project status
Finished