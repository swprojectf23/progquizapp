import 'package:flutter/material.dart';
import 'package:proqquizapp/main.dart';
import 'package:responsive_navigation_bar/responsive_navigation_bar.dart';
import 'game_mode_select.dart';

//Y
double calculateFontSize(String text) {
  double fontSize;
  int lineBreakCount = text.split(RegExp(r'\n')).length;

  if (lineBreakCount <= 2) {
    fontSize = 18;
  } else if (lineBreakCount > 2 && lineBreakCount <= 6) {
    fontSize = 25;
  } else {
    fontSize = 16;
  }
  return fontSize;
}

// Y -TEST
class CustomNavigationBar extends StatelessWidget {
  final int selectedIndex;
  final Function(int) questionDataFunction; // changed to Function(int)

  CustomNavigationBar(
      {required this.selectedIndex, required this.questionDataFunction});

  @override
  Widget build(BuildContext context) {
    return ResponsiveNavigationBar(
      navigationBarButtons: const <NavigationBarButton>[
        NavigationBarButton(
          text: 'Home',
          textColor: Color.fromARGB(255, 240, 240, 240),
          icon: Icons.home,
          backgroundColor: Color.fromARGB(255, 194, 174, 231),
        ),
        NavigationBarButton(
          text: 'Game Mode',
          textColor: Color.fromARGB(255, 240, 240, 240),
          icon: Icons.watch,
          backgroundColor: Color.fromARGB(255, 194, 174, 231),
        ),
      ],
      selectedIndex: selectedIndex,
      onTabChange: (int index) {
        if (index == 0) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => const MyHomePage(),
              ));
        } else if (index == 1) {
          Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) =>
                    GameModeSelect(questionDataFunction: questionDataFunction),
              ));
        }
      },
    );
  }
}

Color getBackgroundColor(bool questionCompleted, bool correctAnswer) {
  if (questionCompleted) {
    if (correctAnswer) {
      return Colors.green;
    }
    return Colors.grey;
  }
  return Colors.white;
}

Color getTextColor(bool questionCompleted) {
  if (questionCompleted) {
    return Colors.white;
  }
  return Colors.black;
}

class QuestionHeader extends StatelessWidget {
  QuestionHeader({super.key, this.flexInt = 3, required this.title});
  final String title;
  final int flexInt;
  final ScrollController _firstController = ScrollController();

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flexInt,
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
        padding: const EdgeInsets.fromLTRB(15, 5, 15, 5),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(20)),
          color: Color(0xFFBBDEFB),
          /*boxShadow: [
                  BoxShadow(
                    spreadRadius: 1,
                    blurRadius: 15,
                    offset: Offset(0, 15)
                  )
                ]*/
        ),
        child: Scrollbar(
          thumbVisibility: true,
          trackVisibility: true,
          thickness: 5,
          controller: _firstController,
          radius: const Radius.circular(5),
          child: SingleChildScrollView(
            controller: _firstController,
            child: Text(
              title,
              style: const TextStyle(
                fontSize: 14,
                fontFamily: 'Consolas',
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class QuestionText extends StatelessWidget {
  const QuestionText({super.key, this.flexInt = 0, required this.text});
  final String text;
  final int flexInt;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: flexInt,
      child: Container(
        margin: const EdgeInsets.fromLTRB(10, 5, 10, 5),
        padding: const EdgeInsets.fromLTRB(10, 10, 10, 10),
        decoration: const BoxDecoration(
          borderRadius: BorderRadius.all(Radius.elliptical(5, 20)),
          color: Color(0xFFE0E0E0),
          /*boxShadow: [
            BoxShadow(
              spreadRadius: 1,
              blurRadius: 10,
              offset: Offset(0, 5)
            )
          ]*/
        ),
        alignment: Alignment.center,
        child: Text(
          text,
          style: TextStyle(
            fontSize: calculateFontSize(text),
          ),
        ),
      ),
    );
  }
}

class QuestionAnswers extends StatelessWidget {
  const QuestionAnswers({
    super.key,
    required this.pressFunctionCorrect,
    required this.pressFunctionWrong,
    required this.answers,
    required this.completion,
  });

  final Function pressFunctionCorrect;
  final Function pressFunctionWrong;
  final List answers;
  final bool completion;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 0,
      child: Align(
        alignment: Alignment.bottomCenter,
        child: ListView.builder(
          physics: const NeverScrollableScrollPhysics(),
          itemCount: 4,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            final buttonIndex = index + 1;
            final buttonText = answers[index];
            if (buttonIndex == int.parse(answers[4])) {
              return ElevatedButton(
                onPressed: () {
                  pressFunctionCorrect();
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: getBackgroundColor(
                      completion, buttonIndex == int.parse(answers[4])),
                ),
                child: Text(
                  buttonText,
                  style: TextStyle(
                    color: getTextColor(completion),
                  ),
                ),
              );
            } else {
              return ElevatedButton(
                onPressed: () {
                  pressFunctionWrong();
                },
                style: ElevatedButton.styleFrom(
                  backgroundColor: getBackgroundColor(
                      completion, buttonIndex == int.parse(answers[4])),
                ),
                child: Text(
                  buttonText,
                  style: TextStyle(
                    color: getTextColor(completion),
                  ),
                ),
              );
            }
          },
        ),
      ),
    );
  }
}
