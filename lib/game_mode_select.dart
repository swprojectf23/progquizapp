import 'package:flutter/material.dart';
import 'package:proqquizapp/leaderboard.dart';
import 'custom_widgets.dart';
import 'main.dart';
import 'timetrial_page.dart';
import 'versus_page.dart';

class GameModeSelect extends StatefulWidget {
  final Function(int) questionDataFunction;

  const GameModeSelect({Key? key, required this.questionDataFunction})
      : super(key: key);

  @override
  State<GameModeSelect> createState() => GameModeSelectPageState();
}

class GameModeSelectPageState extends State<GameModeSelect> {
  bool isCategorySelect = false;
  List<String> categoryText = ['', '', ''];

  @override
  Widget build(BuildContext context) {
    //int Gamemode = 0;

    return Scaffold(
      appBar: AppBar(
        title: const Text(''),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => const MyHomePage()),
            );
          },
        ),
      ),
      bottomNavigationBar: CustomNavigationBar(
        selectedIndex: 1,
        questionDataFunction: (int index) {},
      ),
      body: Center(
        child: isCategorySelect
            ? ListView.builder(
                itemCount: categoryText.length,
                itemBuilder: (context, index) {
                  if (categoryText[index] != '') {
                    return ElevatedButton(
                      onPressed: () {
                        goalPoints = [5, 10, 20][index];
                        category = index;
                        if (gameMode == 0) {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => TimeTrialQuestionPage(
                                    questionDataFunction:
                                        widget.questionDataFunction)),
                          );
                        } else {
                          if (categoryText[index] != 'Ranked') {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => VersusMode(
                                    questionDataFunction:
                                        widget.questionDataFunction,
                                    storage: RankStorage()),
                              ),
                            );
                          }
                        }
                      },
                      child: Text(categoryText[index]),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: categoryText[index] == 'Ranked'
                            ? Colors.grey
                            : null,
                      ),
                    );
                  }
                  return null;
                })
            : Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Text(
                    'Select your game mode',
                    style: TextStyle(fontSize: 24),
                  ),
                  const SizedBox(height: 16),
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        isCategorySelect = true;
                        gameMode = 0;
                        categoryText[0] = '5 Questions';
                        categoryText[1] = '10 Questions';
                        categoryText[2] = '20 Questions';
                      });
                    },
                    child: const Text('Time Trial'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      setState(() {
                        isCategorySelect = true;
                        gameMode = 1;
                        categoryText[0] = "Casual";
                        categoryText[1] = "Ranked";
                      });
                    },
                    child: const Text('Versus'),
                  ),
                ],
              ),
      ),
    );
  }
}
