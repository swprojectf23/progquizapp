import 'package:flutter/material.dart';

import 'dart:math';
import 'dart:convert';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:proqquizapp/game_mode_select.dart';
import 'timetrial_page.dart';


String filename = 'map_data.json';
FirebaseStorageService storageService = FirebaseStorageService();

Map<String, double> mapData = {};

Random random = Random();
int gameMode = 0;
int category = 0;

class MyApp extends StatelessWidget {
  final Function(int) questionDataFunction;
  
  const MyApp({super.key, required this.questionDataFunction});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Button Example',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: Leaderboard(questionDataFunction: questionDataFunction),
    );
  }
}

class Leaderboard extends StatefulWidget {
  final Function(int) questionDataFunction;

  const Leaderboard({super.key, required this.questionDataFunction});

  @override
  LeaderboardPage createState() => LeaderboardPage();
}

class LeaderboardPage extends State<Leaderboard> {
  bool submitted = true;
  final String title = "Leaderboard";
  final TextEditingController _textEditingController = TextEditingController();

  FirebaseStorageService storageService = FirebaseStorageService();

  @override
  void initState(){
    super.initState();
    mapData = {};
    downloadLeaderboard();
  }

  Future<void> downloadLeaderboard() async{
    filename = 'TimeTrial$category.json';
    Map<String, double>? downloadedMapData = await storageService.downloadMapFromFirebaseStorage(filename);
    if (downloadedMapData != null) {
      setState(() {
        mapData = downloadedMapData;
      });
      mapData = await sortMapByValue(mapData);
    } else {
      //ignore: avoid_print
      print('Failed to download map data');
    }
  }

  @override
  Widget build(BuildContext context) {
    // ignore: avoid_print
    print(goalTime);
    double score = goalTime;
    String scoreString = score.toStringAsFixed(3);
    return WillPopScope(
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(title),
          actions: [
            IconButton(
              icon: const Icon(Icons.subdirectory_arrow_right_sharp),
              onPressed: () {
                Navigator.push(
                  context, 
                  MaterialPageRoute(builder: (context) => GameModeSelect(questionDataFunction: widget.questionDataFunction)),
                );
              },
            ),
          ],
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Score: $scoreString",
                style: const TextStyle(fontSize: 30.0),
              ),
              TextField(
                enabled: submitted,
                maxLength: 22,
                controller: _textEditingController,
                decoration: const InputDecoration(
                  labelText: 'Enter your name:',
                ),
              ),
              const SizedBox(height: 16.0),
              ElevatedButton(
                onPressed: () async{
                  if (_textEditingController.text != ""){
                    if ((mapData[_textEditingController.text] != null &&
                    mapData[_textEditingController.text]! >= score) ||
                    mapData[_textEditingController.text] == null) {
                      filename = 'TimeTrial$category.json';
                      submitted = false;
                      mapData[_textEditingController.text] = score;
                      await storageService.uploadMapToFirebaseStorage(mapData, filename);

                      Map<String, double>? downloadedMapData = await storageService.downloadMapFromFirebaseStorage(filename);
                      if (downloadedMapData != null) {
                        setState(() {
                          mapData = downloadedMapData;
                        });
                        mapData = await sortMapByValue(mapData);
                      } else {
                        // ignore: avoid_print
                        print('Failed to download map data');
                      }

                      _textEditingController.text = "";
                    }
                  }
                },
                child: const Text('Submit'),
              ),
              Expanded(
                child: ListView.builder(
                  itemCount: mapData.length,
                  itemBuilder: (context, index) {
                    String key = mapData.keys.elementAt(index);
                    double value = mapData[key]!;
                    String valueString = value.toStringAsFixed(3);
                    index = index + 1;
                    
                    return Row(
                        children: [
                          Text(
                            '  #$index  ',
                            textAlign: TextAlign.left,
                            style: const TextStyle(fontSize: 20.0),
                          ),
                          Expanded(
                            child: Text(
                            textAlign: TextAlign.start,
                            key,
                            style: const TextStyle(fontSize: 20.0),
                            ),
                          ),
                          Expanded(
                            child: Text(
                              '$valueString   -',
                              textAlign: TextAlign.right,
                              style: const TextStyle(fontSize: 20.0),
                            ),
                          ),
                        ],
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FirebaseStorageService {
  final FirebaseStorage _storage = FirebaseStorage.instance;

  Future<void> uploadMapToFirebaseStorage(Map<String, double> mapData, String filename) async {
    try {
      final jsonString = json.encode(mapData);
      final ref = _storage.ref().child(filename);
      final uploadTask = ref.putString(jsonString);
      await uploadTask.whenComplete(() {
        // ignore: avoid_print
        print('Map data uploaded to Firebase Storage successfully.');
      });
    } catch (e) {
      // ignore: avoid_print
      print('Error uploading map data: $e');
    }
  }

  Future<Map<String, double>?> downloadMapFromFirebaseStorage(String filename) async {
    try {
      final ref = _storage.ref().child(filename);
      final downloadUrl = await ref.getDownloadURL();
      final response = await _storage.refFromURL(downloadUrl).getData();
      final jsonString = utf8.decode(response!);
      final mapData = json.decode(jsonString) as Map<String, dynamic>;
      final mapDataTyped = mapData.map((key, value) => MapEntry(key, value as double));
      return mapDataTyped;
    } catch (e) {
      // ignore: avoid_print
      print('Error downloading map data: $e');
      return null;
    }
  }
}

Future<Map<String, double>> sortMapByValue(Map<String, double> inputMap) async{
  List<MapEntry<String, double>> sortedEntries = inputMap.entries.toList()
    ..sort((a, b) => a.value.compareTo(b.value));

  Map<String, double> sortedMap = {};
  for (var entry in sortedEntries) {
    sortedMap[entry.key] = entry.value;
  }

  return sortedMap;
}