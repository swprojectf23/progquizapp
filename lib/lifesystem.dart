import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:another_flushbar/flushbar.dart';
import 'package:shared_preferences/shared_preferences.dart';

int lives = 5;

class LifeSystemPage extends StatefulWidget {
  final double iconSize;

  const LifeSystemPage({
    super.key,
    required this.iconSize,
  });

  @override
  State<LifeSystemPage> createState() => _LifeSystemPageState();
}

class _LifeSystemPageState extends State<LifeSystemPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Consumer<LifeSystemProvider>(
        builder: (context, lifeSystem, child) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                flex: 1,
                child:
              Container(
                color: const Color(0xFFF2F2F7),
                child: Row(
                  children: List.generate(
                    lives,
                    (index) => Icon(
                      Icons.favorite,
                      size: widget.iconSize,
                      color: index < lifeSystem.livesRemaining
                          ? Colors.red
                          : Colors.grey,
                    ),
                  ),
                ),
              ),
              ),
            ],
          );
        },
      ),
    );
  }
}

class LifeSystemProvider with ChangeNotifier {
  int _livesRemaining = lives;
  bool timerRunning = false;
  late Timer? _timer;
  int timeRemaining = 60;

  LifeSystemProvider() {
    _loadLivesAndTime();
  }

  Future<void> _loadLivesAndTime() async {
    await _loadLives();
    await _loadTimeRemaining();
    if (_livesRemaining < lives) {
      timerRunning = true;
      _startTimer();
    }
  }

  Future<void> _saveLives() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('livesRemaining', livesRemaining);
  }

  Future<int> _loadLives() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return _livesRemaining = preferences.getInt('livesRemaining') ?? lives;
  }

  Future<void> _saveTimeRemaining() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.setInt('timeRemaining', timeleft);
  }

  Future<int> _loadTimeRemaining() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    return timeRemaining = preferences.getInt('timeRemaining') ?? timeRemaining;
  }

  int get livesRemaining => _livesRemaining;

  void _startTimer() {
    const seconds = Duration(seconds: 1);
    _timer = Timer.periodic(seconds, (Timer timer) {
      if (timeRemaining > 0 && _livesRemaining < lives) {
        timeRemaining--;
        notifyListeners();
      } else {
        if (_livesRemaining < lives) {
          _livesRemaining++;
          notifyListeners();
        }
        timeRemaining = 60;
        notifyListeners();
      }
      _saveLives();
      _saveTimeRemaining();
    });
  }

  void wrongAnswer() {
    if (_livesRemaining > 0) {
      _livesRemaining--;
      notifyListeners();
      if (_livesRemaining < lives) {
        if (!timerRunning) {
          timerRunning = true;
          _startTimer();
        } else {
          timeRemaining += 0;
        }
      }
    }
    _saveLives();
    _saveTimeRemaining();
  }

  int get timeleft => timeRemaining;

  late String text;

  String _formatTime(int seconds) {
    final minutes = seconds ~/ 60;
    final remainingSeconds = seconds % 60;

    return '${_padMinutesZero(minutes)}:${_padSecondsZero(remainingSeconds)}';
  }

  String _padSecondsZero(int seconds) {
    return seconds.toString().padLeft(2, '0');
  }

  String _padMinutesZero(int minutes) {
    return minutes.toString().padLeft(2, '0');
  }

  String textForHeartsRemaing() {
    int hearts = _livesRemaining;

    if (hearts == 0) {
      text =
          'You ran out of hearts. To solve questions, you need at least one heart';
      return text;
    } else {
      text =
          'You still have $hearts hearts remaining. To solve questions, you need at least one heart';
      return text;
    }
  }

  String textForTimeRemaing() {
    int time = timeleft;
    int hearts = _livesRemaining;

    if (hearts < 5) {
      return 'Next Heart in ${_formatTime(time)}';
    }

    return 'Full Hearts';
  }

  Future<void> livesAndTimeRemaining(BuildContext context) {
    return showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Consumer<LifeSystemProvider>(
            builder: (context, lifeSystem, child) {
          return Padding(
            padding: const EdgeInsets.fromLTRB(20, 0, 20, 100),
            child: SizedBox(
              width: 370,
              height: 510,
              child: Column(
                children: [
                  Align(
                    alignment: Alignment.topRight,
                    child: IconButton(
                      icon: const Icon(Icons.close),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Expanded(
                    flex: 3,
                    child: LifeSystemPage(iconSize: 60.0),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    textForTimeRemaing(),
                    style: const TextStyle(
                      fontSize: 39,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Text(
                    textForHeartsRemaing(),
                    style: const TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
      },
    );
  }

  void outOfHeartsInfo(BuildContext context) {
    Flushbar(
      title: "You ran out of Hearts!",
      message: "Press Heart Button to find more information about it",
      flushbarPosition: FlushbarPosition.TOP,
      backgroundColor: Colors.red,
      duration: const Duration(seconds: 10),
      mainButton: ElevatedButton.icon(
        icon: const Icon(
          Icons.favorite,
          color: Colors.red,
        ),
        onPressed: () {
          livesAndTimeRemaining(context);
        },
        label: const Text(''),
      ),
    ).show(context);
  }
}

class LifeButton extends StatefulWidget {
  const LifeButton({Key? key}) : super(key: key);

  @override
  State<LifeButton> createState() => _LifeButtonState();
}

class _LifeButtonState extends State<LifeButton> {
  @override
  Widget build(BuildContext context) {
    return Consumer<LifeSystemProvider>(
      builder: (context, lifeSystem, child) {
        return ElevatedButton.icon(
          onPressed: () {
            Provider.of<LifeSystemProvider>(context, listen: false)
                .livesAndTimeRemaining(context);
          },
          icon: const Icon(
            Icons.favorite,
            color: Colors.red,
          ),
          label: Text('${lifeSystem.livesRemaining}'),
        );
      },
    );
  }
}
