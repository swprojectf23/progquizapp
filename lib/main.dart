import 'package:flutter/material.dart';
import 'package:proqquizapp/question_page.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'custom_widgets.dart';
import 'firebase_options.dart';
import 'lifesystem.dart';
import 'package:animated_splash_screen/animated_splash_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(
    MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context) => CompletedListProvider()),
        ChangeNotifierProvider(create: (context) => LifeSystemProvider()),
      ],
      child: const MyApp(),
    ),
  );
}

//Yahya_Test
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const SplashScreen(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  final String title = "Level Select";

  List<String> questionData(pageNumber) {
    List<String> list = [];

    switch (pageNumber) {
      case 0:
        list.add("for(int i = 0; i < 10; i++)\n{\n\n}");
        list.add("What language is this code written in?");
        list.add("Python");
        list.add("Assembly");
        list.add("Java");
        list.add("BashScript");
        list.add("3");
        list.add(
            """This code snippet is from an object-oriented programming language (OOP). 
            \nAssembly is a low-level programming language that manipulates the hardware and Bash is a scripting and command language. Neither are considered object-oriented.
            """);
        list.add(
            "Of these options, this syntax will only work (will not create a compile-time error) in Java.");
        break;
      case 1:
        list.add("int x = '10' \nint x = 10.0 \nint x = 10 \ninteger x = 10");
        list.add(
            "Which of the following code snippets correctly declares a variable of type integer with the value 10 in Dart?");
        list.add("int x = '10'");
        list.add("int x = 10");
        list.add("int x = 10.0");
        list.add("integer x = 10");
        list.add("2");
        list.add(
            """Think about what makes the types different. What is the difference between '10', 10.0 and 10 and what are you trying to assign the variable? 
            \nAlso, make sure to choose the right keyword for integer.
            """);
        list.add(
            "In Dart, integers are marked as \"int\" and cannot be declared with decimals (that's a float) or apostrophes (that's a String).");
        break;
      case 2:
        list.add(
            "void main() {\nString? name;\nint len = name?.length ?? 0;\nprint(len);\n}");
        list.add("What does this Dart code print?");
        list.add("null");
        list.add("1");
        list.add("Compilation Error");
        list.add("0");
        list.add("4");
        list.add("""This question revolves around Dart's null safety.
            \nString? makes it so name can now be 'null'.
            \nname? makes it so name.length evaluates to null, if name is null.
            \nThe '??' returns name.length unless null, whereas it returns 0.
            """);
        list.add(
            "This code utilizes Dart's null-safety. We declare a nullable String named name with no starting value (this assigns the value null). We utilize the conditional property access operator ?. to handle the potential null value of name. When getting the length, this returns null, but we also use the null-coalescing operator ?? which returns the value on the right if the value on the left is null. Thus, this code outputs 0.");
        break;
      case 3:
        list.add("x = 5\nx++\nprint(x)");
        list.add("What does this Python code print?");
        list.add("SyntaxError");
        list.add("5");
        list.add("RuntimeError");
        list.add("6");
        list.add("1");
        list.add(
            "The unary operator '++' works in Java. Does it also work in Python?");
        list.add(
            "The ++ operator, usually used to increment a value by one counts as a SyntaxError in Python. You have to use x += 1 instead.");
        break;
      case 4:
        list.add(
            "class Animal {\n\tpublic void sound() {\n\t\tSystem.out.println(\"Animal makes a sound\");\n\t}\n}\nclass Cat extends Animal {\n\tpublic void sound() {\n\t\tSystem.out.println(\"Cat meows\");\n\t}\n}\npublic class Main {\n\tpublic static void main(String[] args) {\n\t\tAnimal animal = new Cat();\n\t\tanimal.sound();\n\t}\n}");
        list.add("What does this Java code print?");
        list.add("Animal makes a sound");
        list.add("Cat meows");
        list.add("Animal makes a sound\nCat meows");
        list.add("Compilation Error");
        list.add("2");
        list.add(
            "What happens when a child class has a method with the same name as a parent's method?");
        list.add(
            "The Cat's sound() method overrides the parent class's sound() method. So when we instantiate an animal of type Cat, the Cat's method is used in place of the parent class's when called.");
        break;
      case 5:
        list.add(
            "class Main {\npublic static void main(String[] args) {\nint x = 10;\nint y = 20;\n\nx = x + y;\ny = x - y;\nx = x - y;\n\nSystem.out.println(\"x=\" + x + \", y=\" + y);\n}\n}");
        list.add("What does this Java code print?");
        list.add("x=10, y=20");
        list.add("IllegalNumberOperation");
        list.add("x=30, y=30");
        list.add("x=20, y=10");
        list.add("4");
        list.add("Grab a piece of paper if the maths are too hard.");
        list.add(
            "This is actually a way to swap numerical values of two variables without any temporary variable. Any two numbers assigned to x and y will be swapped between them with only these simple arithmetic operators.");
        break;
      case 6:
        list.add(
            "void main() {\ndouble x = 0.1;\ndouble y = 0.2;\n\nprint(x + y);\n}");
        list.add("What does this Dart code print?");
        list.add("0.30000000000000004");
        list.add("0.3");
        list.add("0.30000000000000000");
        list.add("0.10.2");
        list.add("1");
        list.add(
            "Doubles are floating-point decimal numbers. What does floating-point mean? Will they always be accurate?");
        list.add(
            "Due to floating-point imprecision, the code will output a number very close to the desired 0.3, but not quite. This also means that (x + y != 0.3), so watch out!");
        break;
      case 7:
        list.add(
            "class Main {\npublic static void main(String[] args) {\nInteger a = 1000;\nInteger b = 1000;\n\nSystem.out.println(a == b);\n}\n}");
        list.add("What does this Java code print?");
        list.add("true");
        list.add("Error: Incomparable Types");
        list.add("null");
        list.add("false");
        list.add("4");
        list.add("What's the difference between an Integer and an int?");
        list.add(
            "The reason for this behavior is that Integer objects in Java are compared using reference equality when using the == operator. Although the values of a and b are the same, they are different objects, and the == operator compares their references instead of their values. For an expected result, use the equals() method of Integer or use the primitive type int.");
      case 8:
        list.add(
            "void main() {\nList<String> fruits = ['Apple', 'Banana', 'Orange'];\n\nList<String> fruitBasket = fruits;\nfruitBasket.add('Mango');\n\nprint(fruits.length);\n}");
        list.add("What does this Dart code print?");
        list.add("[Apple, Banana, Orange]");
        list.add("3");
        list.add("4");
        list.add("[Apple, Banana, Orange, Mango]");
        list.add("3");
        list.add(
            "When we assign the fruitBasket, does the fruit list get copied from fruits?");
        list.add(
            "We have a list of three fruits called fruits. We then create a new list (fruitBasket) and assign it the reference of the fruits list. Both fruits and fruitBasket now refer to the same list in memory. So, when we add a new fruit to the fruitBasket, it also gets added to the fruits list since they are the same list, but just with two different names.");
        break;
      case 9:
        list.add(
            "void main() {\nList<int> integerList = [1, 2, 3];\n\nList<String> stringList = ['Code', 'Crunch', 'Rocks'];\n\nList myFirstList = stringList;\n\nList<num> numberList = integerList;\n\nList mySecondList = numberList;\n}");
        list.add("How many lists does this program create in memory?");
        list.add("3");
        list.add("2");
        list.add("5");
        list.add("Error: Incorrect types for value assignment.");
        list.add("2");
        list.add(
            "Which variables are references to lists, and which are actually lists?");
        list.add(
            "Only two unique lists exist in memory. They are declared at the top as integerList and stringList. The rest of the lists are simply new references to these original two lists. Thus, only two lists actually exist in memory.");
        break;
      case 10:
        list.add(
            "import 'package:flutter/material.dart';\n\nvoid main() {\n  runApp(MyApp());\n}\n\nclass MyApp extends StatelessWidget {\n  @override\n  Widget build(BuildContext context) {\n    return MaterialApp(\n      title: 'Flutter CodeBite',\n      home: Scaffold(\n        appBar: AppBar(\n          title: Text('CodeBite'),\n        ),\n        body: Center(\n          child: Text(\n            'Hello, World!',\n            style: TextStyle(fontSize: 24),\n          ),\n        ),\n      ),\n    );\n  }\n}");
        list.add("What does this Flutter code do?");
        list.add("Displays a simple \"Hello, World!\" message.");
        list.add("Creates a Flutter app with a basic user interface.");
        list.add("Sets up an app with an AppBar.");
        list.add("All of the above.");
        list.add("4");
        list.add(
            "Flutter is a framework for developing applications. Read through the code from the top. What does it do?");
        list.add(
            "That's right. This simple codebite creates an entire app to run on basically any device which displays a \"Hello World!\" message with an AppBar reminiscent of contemporary Android apps.");
        break;
      case 11:
        list.add(
            "import 'package:flutter/material.dart';\n\nvoid main() {\n  runApp(MyApp());\n}\n\nclass MyApp extends StatelessWidget {\n  @override\n  Widget build(BuildContext context) {\n    return MaterialApp(\n      home: Scaffold(\n        appBar: AppBar(\n          title: Text('Column Demo'),\n        ),\n        body: Center(\n          child: Column(\n            mainAxisAlignment: MainAxisAlignment.center,\n            children: <Widget>[\n              Text('Widget 1'),\n              Text('Widget 2'),\n              Text('Widget 3'),\n            ],\n          ),\n        ),\n      ),\n    );\n  }\n}");
        list.add(
            "What does the mainAxisAlignment property of the Column widget control?");
        list.add("Controls horizontal alignment of the children.");
        list.add("Controls vertical alignment of the children.");
        list.add("Controls spacing between the children.");
        list.add("It sets the height of the children.");
        list.add("2");
        list.add(
            "A Column is a collection of child widgets arranged vertically on top of each other. In this context, what is the \"main axis\"?");
        list.add(
            "For a Column, the \"main axis\" is the vertical axis. Thus, the alignment along the main axis specifies the vertical alignment of the elements in the Column.");
        break;
      case 12:
        list.add(
            "import 'package:flutter/material.dart';\n\nvoid main() {\n  runApp(MyApp());\n}\n\nclass MyApp extends StatelessWidget {\n  @override\n  Widget build(BuildContext context) {\n    return MaterialApp(\n      home: Scaffold(\n        appBar: AppBar(\n          title: Text('Row Demo'),\n        ),\n        body: Center(\n          child: Row(\n            mainAxisAlignment: MainAxisAlignment.spaceEvenly,\n            crossAxisAlignment: CrossAxisAlignment.center,\n            children: <Widget>[\n              Text('Widget 1'),\n              Text('Widget 2'),\n              Text('Widget 3'),\n            ],\n          ),\n        ),\n      ),\n    );\n  }\n}\n");
        list.add(
            "What does the crossAxisAlignment property of the Row widget control?");
        list.add("Controls horizontal alignment of the children.");
        list.add("Controls vertical alignment of the children.");
        list.add("Controls spacing between the children.");
        list.add("It sets the height of the children.");
        list.add("2");
        list.add(
            "A Row is a collection of child widgets arranged horizontally next to each other. In this context, what is the \"cross axis\"? Think about which axis would cross the main axis.");
        list.add(
            "For a Row, the \"cross axis\" is the vertical axis. Thus, the alignment along the cross axis specifies the vertical alignment of the elements in the Row.");
        break;
      case 13:
        list.add(
            "import 'package:flutter/material.dart';\n\nvoid main() {\n  runApp(MyApp());\n}\n\nclass MyApp extends StatelessWidget {\n  @override\n  Widget build(BuildContext context) {\n    return MaterialApp(\n      home: Scaffold(\n        appBar: AppBar(\n          title: Text('SizedBox Example'),\n        ),\n        body: Center(\n          child: Column(\n            mainAxisAlignment: MainAxisAlignment.center,\n            children: [\n              Text('CodeCrunch!'),\n              SizedBox(height: 20),\n              Text('SizedBox example'),\n            ],\n          ),\n        ),\n      ),\n    );\n  }\n}\n");
        list.add("What is the purpose of the SizedBox widget in Flutter?");
        list.add("To add padding around child widgets.");
        list.add("To adjust the width and height of child widgets.");
        list.add("To create a separator between child widgets.");
        list.add("To apply a background color to child widgets.");
        list.add("3");
        list.add(
            "We specify a height for a SizedBox. What happens when we insert it into a Column widget?");
        list.add(
            "With SizedBoxes, you can specify specific widths and heights of an invisible box. In this example, we insert a SizedBox into a Column, creating space between the other elements.");
        list.add(
            "We specify a height for a SizedBox. What happens when we insert it into a Column widget?");
        list.add(
            "With SizedBoxes, you can specify specific widths and heights of an invisible box. In this example, we insert a SizedBox into a Column, creating space between the other elements.");
        break;
      case 14:
        list.add(
            "import 'dart:math';\n\nvoid main() {\n var randomNumber = generateRandomNumber();\n print('Random number: \$randomNumber');\n}\n\nint generateRandomNumber() {\n var random = Random();\n return random.nextInt(100);\n}");
        list.add("What is the purpose of the 'Random' class in Dart?");
        list.add("To generate random numbers.");
        list.add("To sort a list of elements in random order.");
        list.add("To shuffle the characters in a string randomly.");
        list.add("To generate random boolean values.");
        list.add("1");
        list.add(
            "Hint: The 'Random' class provides methods to generate random numbers.");
        list.add(
            "In this code, we import the 'dart:math' library to access the 'Random' class. The 'Random' class is used to generate random numbers. We create an instance of the 'Random' class and call the 'nextInt' method to generate a random number between 0 and 99. The generated random number is then printed to the console.");
        break;
      case 15:
        list.add(
            "void main() {\n var stringList = ['apple', 'banana', 'cherry'];\n var result = concatenateStrings(stringList);\n print('Result: \$result');\n}\n\nString concatenateStrings(List<String> strings) {\n var result = '';\n for (var str in strings) {\n result += str;\n }\n return result;\n}");
        list.add(
            "What is the purpose of the 'concatenateStrings' function in the code?");
        list.add("To convert a string to uppercase.");
        list.add("To split a string into multiple substrings.");
        list.add("To combine multiple strings into a single string.");
        list.add("To remove whitespace characters from a string.");
        list.add("3");
        list.add(
            "Hint: The 'concatenateStrings' function takes a list of strings as input.");
        list.add(
            "In this code, the 'concatenateStrings' function is defined to take a list of strings as input. It initializes an empty string 'result' and then iterates over each string in the 'strings' list. It appends each string to the 'result' string using the '+=' operator. Finally, it returns the concatenated 'result' string. The 'main' function demonstrates the usage of 'concatenateStrings' by passing a list of strings and printing the concatenated result to the console.");
        break;
      case 16:
        list.add(
            "void main() {\n var numberList = [1, 2, 3, 4, 5];\n var elementList = ListElements(numberList, 2);\n print('elemental List: \$elementList');\n}\n\nList<int> ListElements(List<int> numbers, int number) {\n var elementList = [];\n for (var num in numbers) {\n elementList.add(num * number);\n }\n return elementList;\n}");
        list.add(
            "What is the purpose of the 'ListElements' function in the code?");
        list.add("To sort a list of numbers in ascending order.");
        list.add("To divide each element of a list by a given divisor.");
        list.add("To filter out odd numbers from a list.");
        list.add("To multiply each element of a list by a given multiplier.");
        list.add("4");
        list.add(
            "Hint: The 'multiplyListElements' function takes a list of numbers and a multiplier as input.");
        list.add(
            "In this code, the 'ListElements' function is defined to take a list of integers and a number as input. It creates an empty list 'elementList' and then iterates over each number in the 'numbers' list. it returns the 'elementList' with the elements multiplied by the variable 'number'. The 'main' function demonstrates the usage of 'ListElements' by passing a list of numbers and a multiplier, and printing the resulting multiplied list to the console.");
        break;
      case 17:
        list.add(
            "void main() {\n var sentence = 'Hello, how are you today?';\n var varSentence = varSentence(sentence);\n print('\$varSentence');\n}\n\nString varSentence(String sentence) {\n var words = sentence.split(' ');\n var varWords = words.reversed.toList();\n var varSentence = varWords.join(' ');\n return varSentence;\n}");
        list.add(
            "What is the purpose of the 'varSentence' function in the code?");
        list.add("To count the number of words in a sentence.");
        list.add("To reverse the characters in each word of a sentence.");
        list.add("To remove punctuation marks from a sentence.");
        list.add("To reverse the order of words in a sentence.");
        list.add("4");
        list.add(
            "Hint: The 'varSentence' function utilizes string splitting and joining operations.");
        list.add(
            "The 'varSentence' function takes a sentence as input and reverses the order of its words. It accomplishes this by splitting the sentence into words, reversing the order of the words, and then joining them back together. The reversed sentence is then returned. The 'main' function showcases the usage of 'varSentence' by providing a sentence and printing the reversed sentence to the console.");
        break;
      case 18:
        list.add(
            "void main() {\n var matrix = [\n [1, 2, 3],\n [4, 5, 6],\n [7, 8, 9]\n ];\n\n var varMatrix = varMatrix(matrix);\n\n print('Matrix:');\n for (var row in varMatrix) {\n print(row);\n }\n}\n\nList<List<int>> varMatrix(List<List<int>> matrix) {\n var rows = matrix.length;\n var columns = matrix[0].length;\n\n var varMatrix = List.generate(columns, (index) => List.filled(rows, 0));\n\n for (var i = 0; i < rows; i++) {\n for (var j = 0; j < columns; j++) {\n varMatrix[j][i] = matrix[i][j];\n }\n }\n\n return varMatrix;\n}");
        list.add(
            "What is the purpose of the 'varMatrix' function in the code?");
        list.add("To multiply two matrices together.");
        list.add("To calculate the determinant of a square matrix.");
        list.add("To transpose a matrix by swapping its rows and columns.");
        list.add("To find the inverse of a matrix.");
        list.add("3");
        list.add(
            "Hint: The 'varMatrix' function generates a new matrix with swapped rows and columns.");
        list.add(
            "The 'varMatrix' function swaps the rows and columns of a matrix. It creates a new matrix with the dimensions reversed by iterating over the original matrix and assigning elements to the transposed matrix. The transposed matrix is returned. The 'main' function demonstrates the usage of 'varMatrix' by passing a matrix and printing the transposed matrix row by row.");
      case 19:
        list.add(
            "void main() {\n var variable1 = object1();\n\n object1.insert(5);\n object1.insert(3);\n object1.insert(8);\n object1.insert(2);\n object1.insert(4);\n object1.insert(7);\n object1.insert(9);\n\n print('Inorder Traversal:');\n object1.function3();\n}\n\nclass object2 {\n int value;\n object2 left;\n object2 right;\n\n object2(this.value);\n}\n\nclass object1 {\n object2 root;\n\n void insert(int value) {\n root = function2(root, value);\n }\n\n object2 function2(object2 node, int value) {\n if (node == null) {\n return object2(value);\n }\n\n if (value < node.value) {\n node.left = function2(node.left, value);\n } else if (value > node.value) {\n node.right = function2(node.right, value);\n }\n\n return node;\n }\n\n void function3() {\n function1(root);\n }\n\n void function1(TreeNode node) {\n if (node != null) {\n function1(node.left);\n print(node.value);\n function1(node.right);\n }\n }\n}");
        list.add(
            "What is the purpose of the 'object1' class and its associated methods in the code?");
        list.add(
            "To represent and perform operations on a binary search tree.");
        list.add("To perform sorting operations on an array of integers.");
        list.add("To perform mathematical computations on binary numbers.");
        list.add("To implement a queue data structure using a binary tree.");
        list.add("1");
        list.add(
            "Hint: The 'object1' class represents a binary search tree and provides methods for insertion and traversal.");
        list.add(
            "The 'object1' class represents a binary search tree with a 'root' property of type 'object2'. The 'insert' method recursively inserts a new node into the tree based on the given value. The 'function3' method performs an ascending order traversal, printing the values. Private methods 'function2' and 'function1' handle the recursive logic. The 'main' function showcases the usage by creating a tree, inserting values, and printing the traversal.");
        break;
      default:
        list.add("Question text not found!");
        list.add("Question text not found!");
        list.add("Option 1");
        list.add("Option 2");
        list.add("Option 3");
        list.add("Option 4");
        list.add("1");
        list.add("This is a hint!");
        list.add("You shouldn't see this...");
        break;
    }

    return list;
  }

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  void initState() {
    super.initState();
    Provider.of<CompletedListProvider>(context, listen: false).loadLevels();
  }

  @override
  Widget build(BuildContext context) {
    const int levelCount = 20;

    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: Center(child: Text(widget.title)),
        actions: const [
          LifeButton(),
        ],
      ),
      body: Center(
          child: ListView(
        children: List.generate(levelCount, (index) {
          return GestureDetector(
            onTap: () {
              if (Provider.of<LifeSystemProvider>(context, listen: false)
                      .livesRemaining >
                  0) {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => QuestionPage(
                      pageTitle: 'Question ${index + 1} / $levelCount',
                      pageNumber: index,
                      questionDataFunction: widget.questionData,
                    ),
                  ),
                );
              } else {
                Provider.of<LifeSystemProvider>(context, listen: false)
                    .outOfHeartsInfo(context);
              }
            },
            child: ListTile(
              leading: const Icon(Icons.question_mark,
                  size: 30, color: Color.fromARGB(255, 21, 178, 170)),
              title: Text("Question number: ${index + 1}"),
              trailing: Icon(
                (Provider.of<CompletedListProvider>(context)
                        .completedList[index])
                    ? Icons.check
                    : Icons.close,
                color: (Provider.of<CompletedListProvider>(context)
                        .completedList[index])
                    ? Colors.green
                    : Colors.red,
              ),
            ),
          );
        }),
      )),
      bottomNavigationBar: CustomNavigationBar(
        selectedIndex: 0,
        questionDataFunction: widget.questionData,
      ),
    );
  }
}

class CompletedListProvider extends ChangeNotifier {
  List<bool> completedList = List.generate(20, (_) => false);

  void updateBooleanList(int index, bool value) {
    completedList[index] = value;
    notifyListeners();
  }

  Future<void> saveLevels() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    for (int i = 0; i < 20; i++) {
      await preferences.setBool('levelCompleted$i', completedList[i]);
    }
  }

  Future<void> loadLevels() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    List<bool> list = [];
    for (int i = 0; i < 20; i++) {
      list.add(preferences.getBool('levelCompleted$i') ?? false);
    }

    completedList = list;
    notifyListeners();
  }
}

class SplashScreen extends StatelessWidget {
  const SplashScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return AnimatedSplashScreen(
      splash: 'assets/Logo.jpg',
      backgroundColor: Colors.black,
      splashIconSize: 300,
      duration: 2000,
      splashTransition: SplashTransition.fadeTransition,
      animationDuration: const Duration(seconds: 1),
      nextScreen: const MyHomePage(),
    );
  }
}
