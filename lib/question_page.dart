import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:proqquizapp/custom_widgets.dart';
import 'package:provider/provider.dart';
import 'main.dart';
import 'package:vibration/vibration.dart';
import 'package:audioplayers/audioplayers.dart';
import 'lifesystem.dart';

class QuestionPage extends StatefulWidget {
  final String pageTitle;
  final int pageNumber;
  final Function(int) questionDataFunction;

  const QuestionPage(
      {super.key,
      required this.pageTitle,
      required this.pageNumber,
      required this.questionDataFunction});

  @override
  State<QuestionPage> createState() => _QuestionPageState();
}

class _QuestionPageState extends State<QuestionPage> {
  bool hintActive = false;

  void toggleHint() {
    if (hintActive) {
      hintActive = false;
    } else {
      hintActive = true;
    }
  }

  @override
  Widget build(BuildContext context) {
    List<String> data = widget.questionDataFunction(widget.pageNumber);

    IconData icon;
    Color iconColor;
    if (hintActive) {
      icon = Icons.lightbulb;
      iconColor = const Color.fromARGB(255, 255, 200, 0);
    } else {
      icon = Icons.lightbulb_outline;
      iconColor = Colors.black54;
    }

    String questionContent;
    if (!hintActive) {
      questionContent = data[0];
    } else {
      questionContent = 'Hint: \n${data[7]}';
    }

    return Scaffold(
      appBar: AppBar(
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Text(widget.pageTitle),
            GestureDetector(
              onTap: () {
                setState(() {
                  toggleHint();
                });
              },
              child: SizedBox(
                width: 50,
                height: 50,
                child: Icon(
                  icon,
                  color: iconColor,
                  size: 35,
                ),
              ),
            ),
          ],
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const Expanded(
            child: LifeSystemPage(
              iconSize: 30,
            ),
          ),
          QuestionHeader(title: questionContent),
          QuestionText(text: data[1]),
          QuestionAnswers(
              pressFunctionCorrect: () {
                if (Provider.of<LifeSystemProvider>(context, listen: false)
                        .livesRemaining >
                    0) {
                  //Set level as completed
                  setState(() {
                    Vibration.vibrate();
                    AudioPlayer().play(AssetSource('click-success.mp3'));
                    answerExplanation(context, data[8]);

                    Provider.of<CompletedListProvider>(context, listen: false)
                        .updateBooleanList(widget.pageNumber, true);

                    Provider.of<CompletedListProvider>(context, listen: false)
                        .saveLevels();
                  });
                } else {
                  Provider.of<LifeSystemProvider>(context, listen: false)
                      .outOfHeartsInfo(context);
                }
              },
              pressFunctionWrong: () {
                if (Provider.of<LifeSystemProvider>(context, listen: false)
                        .livesRemaining >
                    0) {
                  HapticFeedback.vibrate();
                  AudioPlayer().play(AssetSource('click-error.wav'));

                  if (!Provider.of<CompletedListProvider>(context,
                          listen: false)
                      .completedList[widget.pageNumber]) {
                    Provider.of<LifeSystemProvider>(context, listen: false)
                        .wrongAnswer();
                  }

                  if (Provider.of<LifeSystemProvider>(context, listen: false)
                          .livesRemaining <
                      1) {
                    Provider.of<LifeSystemProvider>(context, listen: false)
                        .livesAndTimeRemaining(context);
                  }
                } else {
                  Provider.of<LifeSystemProvider>(context, listen: false)
                      .outOfHeartsInfo(context);
                }
              },
              answers: data.sublist(2),
              completion: Provider.of<CompletedListProvider>(context)
                  .completedList[widget.pageNumber]),
        ],
      ),
    );
  }

  Future<void> answerExplanation(BuildContext context, String explanation) {
    return showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          color: Colors.green.shade100,
          child: SizedBox(
            height: 250,
            width: 750,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    IconButton(
                      alignment: Alignment.topRight,
                      icon: const Icon(Icons.close),
                      onPressed: () => Navigator.pop(context),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(6, 0, 6, 0),
                  child: Expanded(
                    child: SingleChildScrollView(
                      child: Text(
                        explanation,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      },
    );
  }
}
