import 'dart:async';

import 'package:flutter/material.dart';
import 'package:proqquizapp/leaderboard.dart';
import 'dart:math';
import 'package:flutter/services.dart';
import 'package:vibration/vibration.dart';
import 'package:audioplayers/audioplayers.dart';

import 'custom_widgets.dart';

int point = 0;
int lives = 3;
int goalPoints = 10;

double wrongs = 0.0;
double goalTime = 10000.0;
double _seconds = 0.0;

class TimeTrialQuestionPage extends StatefulWidget {
  final String pageTitle = 'TimeTrial';
  int pageNumber = Random().nextInt(20);
  final Function(int) questionDataFunction;

  TimeTrialQuestionPage({super.key, required this.questionDataFunction});

  @override
  State<TimeTrialQuestionPage> createState() => _TimeTrialQuestionPageState();
}

class _TimeTrialQuestionPageState extends State<TimeTrialQuestionPage> {
  bool questionCompleted = false;
  late Timer _timer;
  final DateTime _startTime = DateTime.now();

  @override
  void initState() {
  point = 0;
  wrongs = 0.0;
  super.initState();
  _startTimer();
}

void _startTimer() {
  const oneMillisecond = Duration(milliseconds: 1);
  _timer = Timer.periodic(oneMillisecond, (Timer timer) {
    setState(() {
      final currentTime = DateTime.now();
      final difference = currentTime.difference(_startTime);
      _seconds = difference.inMilliseconds.toDouble() * 0.001 + wrongs * 5;
    });
  });
}

  void resetQuestion() {
    setState(() {
      questionCompleted = false;
      widget.pageNumber = Random().nextInt(20);
    });
  }

  @override
  void dispose() {
    _timer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    List<String> data = widget.questionDataFunction(widget.pageNumber);
    String time = _seconds.toStringAsFixed(3);

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.pageTitle),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Expanded(
            flex: 0,
            child: Text('Time: $time\n$point/$goalPoints'),
          ),
          QuestionHeader(title: data[0]),
          QuestionText(text: data[1]),
          QuestionAnswers(
            pressFunctionCorrect: () async {
              if(!questionCompleted){
                Vibration.vibrate();
                AudioPlayer().play(AssetSource('click-success.mp3'));
                questionCompleted = true;
                point += 1;
                if (point >= goalPoints) {
                  _timer.cancel();
                }
                goalTime = _seconds;
                if (point >= goalPoints) {
                  point = 0;
                  wrongs = 0.0;
                  // ignore: use_build_context_synchronously
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Leaderboard(questionDataFunction: widget.questionDataFunction)),
                  );
                }
                await Future.delayed(const Duration(milliseconds: 500));
                resetQuestion();
              }
            },
            pressFunctionWrong: () async {
              if(!questionCompleted){
                questionCompleted = true;
                HapticFeedback.vibrate();
                AudioPlayer().play(AssetSource('click-error.wav'));
                wrongs += 1;
                await Future.delayed(const Duration(milliseconds: 500));
                resetQuestion();
              }}, 
            answers: data.sublist(2), 
            completion: questionCompleted, 
          )
        ],
      ),
    );
  }
}