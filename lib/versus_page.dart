import 'dart:io';

import 'package:flutter/material.dart';
import 'package:proqquizapp/versus_page_search.dart';
import 'package:path_provider/path_provider.dart';

class VersusMode extends StatefulWidget {
  final Function(int) questionDataFunction;

  final RankStorage storage;

  const VersusMode({Key? key, required this.questionDataFunction, required this.storage})
      : super(key: key);

  @override
  State<VersusMode> createState() => VersusModePage();
}

class VersusModePage extends State<VersusMode> {
  final TextEditingController _textEditingController = TextEditingController();

  String rankName = '';
  String rankPoints = '1000';

  @override
  void initState() {
    super.initState();
    widget.storage.readData().then((values) {
      setState(() {
        rankName = values.isNotEmpty ? values[0] : '';
        rankPoints = values.isNotEmpty ? values[1] : '1000';
      });
    });
  }

  Future<File> updateRankData() {
    setState(() {
      rankPoints = '1000';
      rankName = _textEditingController.text;
    });

    // Write the variable as a string to the file.
    return widget.storage.writeData(rankName ,rankPoints);
  }

  @override
  Widget build(BuildContext context) {
    _textEditingController.text = rankName;
    bool versusDisplay = false;

    return Scaffold(
      body: Center(
        child: Column(
          children: [
            if (!versusDisplay)
            Container(height: 70),
            Text(
              'CodePoints: $rankPoints',
              style: const TextStyle(
                 fontSize: 20,
              ),
            ),
            
            TextField(
                controller: _textEditingController,
                maxLength: 20,
                decoration: const InputDecoration(
                  labelText: 'Enter your name:',
                ),
            ),

            ElevatedButton(
              onPressed: () async{
                updateRankData();
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => VersusModeSearch(
                    username: rankName, 
                    questionDataFunction: widget.questionDataFunction, 
                    rankPoints: rankPoints,
                  ),
                  ),
                );
              }, 
              child: const Text('Search')
            ),
          ],
        )
      )
    );
  }
}

class RankStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();

    return directory.path;
  }

  Future<File> get _localFile async {
    final path = await _localPath;
    return File('$path/VersusMode.txt');
  }

  Future<List<String>> readData() async {
    try {
      final file = await _localFile;

      // Read the file
      final contents = await file.readAsLines();

      return contents;
    } catch (e) {
      return [];
    }
  }

  Future<File> writeData(String rankName, String rankPoints) async {
    final file = await _localFile;

    // Write the file
    final data = '$rankName\n$rankPoints';
    return file.writeAsString(data);
  }
}