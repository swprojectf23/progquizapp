import 'dart:async';

import 'package:flutter/material.dart';
import 'package:proqquizapp/custom_widgets.dart';
import 'dart:math';
import 'package:flutter/services.dart';
import 'package:proqquizapp/versus_page_winscreen.dart';
import 'package:vibration/vibration.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:firebase_database/firebase_database.dart';

class VersusBattle extends StatefulWidget {
  final String pageTitle = 'TimeTrial';
  int pageNumber = Random().nextInt(20);
  final Function(int) questionDataFunction;
  final List<String> usernames;
  final String username;
  final String serverId;

  VersusBattle({
    super.key, 
    required this.questionDataFunction,
    required this.usernames,
    required this.username,
    required this.serverId,
  });

  @override
  State<VersusBattle> createState() => VersusBattlePage();
}

class VersusBattlePage extends State<VersusBattle> {
  late DatabaseReference serverRef;

  int maxHp1 = 100;
  int maxHp2 = 100;

  int player1Health = 100;
  int player2Health = 100;

  String winName = '';

  bool questionCompleted = false;

  @override
  void initState() {
    serverRef = FirebaseDatabase.instance.reference().child(widget.serverId);
    super.initState();
    listenToHealthChanges();
  }

  void resetQuestion() {
    questionCompleted = false;
    widget.pageNumber = Random().nextInt(20);
  }

  void listenToHealthChanges() {
    final player1Ref = serverRef.child('players/player1');
    final player2Ref = serverRef.child('players/player2');

    player1Ref.onValue.listen((event) {
      final data = event.snapshot.value as Map<dynamic, dynamic>?;
      if (data != null && data['health'] != null) {
        setState(() {
          player1Health = data['health'] as int;
        });
      }
    });

    player2Ref.onValue.listen((event) {
      final data = event.snapshot.value as Map<dynamic, dynamic>?;
      if (data != null && data['health'] != null) {
        setState(() {
          player2Health = data['health'] as int;
        });
      }
    });
  }

  void onHealthChanged(int playerNumber, int newHealth) {
    updateHealth(playerNumber, newHealth);
  }

  Future<void> updateHealth(int playerNumber, int health) async {
    final playerRef = serverRef.child('players/player$playerNumber');
    await playerRef.set({'health': health});
  }

  Color getBackgroundColor(int answerNumber, int correctAnswer) {
    if (questionCompleted) {
      if (answerNumber == correctAnswer) {
        return Colors.green;
      }
      return Colors.grey;
    }
    return Colors.white;
  }

  Color getTextColor(int answerNumber, int correctAnswer) {
    if (questionCompleted) {
      return Colors.white;
    }
    return Colors.black;
  }

  @override
  Widget build(BuildContext context) {
    List<String> data = widget.questionDataFunction(widget.pageNumber);
    String enemyUsername = '';

    int hp1 = 100;
    int hp2 = 100;

    if (widget.username == widget.usernames[0]) {
      enemyUsername = widget.usernames[1];
      hp1 = player1Health;
      hp2 = player2Health;
    } else {
      enemyUsername = widget.usernames[0];
      hp2 = player1Health;
      hp1 = player2Health;
    }
    
    return Scaffold(
      body: Column(
        children: [
          const SizedBox(height: 10),
          Container(
            height: 60,
            width: (350 * hp2 / maxHp2).abs(),
            decoration: BoxDecoration(
              color: Colors.red,
              border: Border.all(
                color: Colors.black,
                width: 1,
              ),
            ),
            child: Center(
              child: Text(
                '$hp2',
                style: const TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),),
            ),
          ),
          Text(
            enemyUsername,
            style: const TextStyle(
              fontSize: 16,
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(top: 0),
              child: ConstrainedBox(
                constraints: const BoxConstraints(
                  minHeight: 200,
                  maxHeight: 200,
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    QuestionHeader(title: data[0]),
                    QuestionText(text: data[1]),
                    QuestionAnswers(
                      pressFunctionCorrect: () async {
                        if(!questionCompleted){
                          // Set level as completed
                          Vibration.vibrate();
                          AudioPlayer().play(AssetSource('click-success.mp3'));
                          questionCompleted = true;
                          setState(() {});
                          await Future.delayed(const Duration(milliseconds: 500));
                          
                          if (widget.username == widget.usernames[1]){
                            player1Health = player1Health - 5;
                            await updateHealth(1, player1Health);
                          } else{
                            player2Health = player2Health - 5;
                            await updateHealth(2, player2Health);
                          }

                          if (player1Health <= 0 || player2Health <= 0){
                            if (widget.username == widget.usernames[0]) {
                              if (player1Health <= 0){
                                winName = enemyUsername;
                              } else{
                                winName = widget.username;
                              }
                            } else{
                              if (player1Health <= 0){
                                winName = widget.username;
                              } else{
                                winName = enemyUsername;
                              }
                            }
                            serverRef.remove();

                            Navigator.push(
                              context,
                              MaterialPageRoute(builder: (context) => VersusModeWinScreen(
                                winName: winName,
                                questionDataFunction: widget.questionDataFunction
                              )),
                            );

                            player1Health = 100;
                            player2Health = 100;
                          }
                          resetQuestion();
                          setState(() {});
                        }
                      },
                      pressFunctionWrong: () async {
                        if(!questionCompleted){
                          questionCompleted = true;
                          HapticFeedback.vibrate();
                          AudioPlayer().play(AssetSource('click-error.wav'));
                          setState(() {});
                          await Future.delayed(const Duration(milliseconds: 500));
                          resetQuestion();
                          setState(() {});
                        }
                      }, 
                      answers: data.sublist(2), 
                      completion: questionCompleted)
                  ],
                ),
              ),
            ),
          ),
          const SizedBox(
            height: 15,
          ),
          Text(
            widget.username,
            style: const TextStyle(
              fontSize: 16,
            ),
          ),
          Container(
            height: 40,
            width:  (350 * hp1 / maxHp1).abs(),
            decoration: BoxDecoration(
              color: Colors.green,
              border: Border.all(
                color: Colors.black,
                width: 1,
              ),
            ),
            child: Center(
              child: Text(
                '$hp1',
                style: const TextStyle(
                  fontSize: 20,
                  color: Colors.white,
                ),),
            ),
          ),
        ],
      ),
    );
  }
}