import 'package:flutter/material.dart';
import 'package:proqquizapp/versus_page_battle.dart';


class VersusModeDisplay extends StatefulWidget {
  final Function(int) questionDataFunction;
  final List<String> usernames;
  final String username;
  final String serverId;

  const VersusModeDisplay({
    super.key, 
    required this.questionDataFunction,
    required this.usernames,
    required this.username,
    required this.serverId,
  });

  @override
  State<VersusModeDisplay> createState() => VersusModeDisplayPage();
}

class VersusModeDisplayPage extends State<VersusModeDisplay> {

  @override
  void initState() {
    startBattle();
    super.initState();
  }
  
  void startBattle() async{
    await Future.delayed(const Duration(seconds: 3));
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => VersusBattle(
          usernames: widget.usernames,
          username: widget.username,
          questionDataFunction: widget.questionDataFunction, 
          serverId: 'Server: ${widget.usernames[0]} vs ${widget.usernames[0]}',
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope( 
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Column(
              children: [
                Text(
                  widget.usernames[0],
                  style: const TextStyle(fontSize: 24,fontWeight: FontWeight.bold),
                ),
                const Text(
                  "VS",
                  style: TextStyle(fontSize: 28,fontWeight: FontWeight.bold,fontStyle: FontStyle.italic),
                ),
                Text(
                  widget.usernames[1],
                  style: const TextStyle(fontSize: 24,fontWeight: FontWeight.bold),
                ),
              ]
            ),
          ),
        ],
      ),
    )
    );
  }
}