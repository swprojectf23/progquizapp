import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:proqquizapp/versus_page_display.dart';

class VersusModeSearch extends StatefulWidget {
  final Function(int) questionDataFunction;
  final String username;
  final String rankPoints;

  const VersusModeSearch({
    Key? key,
    required this.username,
    required this.questionDataFunction,
    required this.rankPoints,
  }) : super(key: key);

  @override
  State<VersusModeSearch> createState() => VersusModeSearchPage();
}

class VersusModeSearchPage extends State<VersusModeSearch> {
  final DatabaseReference usersRef = FirebaseDatabase.instance.reference().child('users');

  List<String> usernames = [];

  @override
  void initState() {
    super.initState();
    listenForUsernames();
  }

  void listenForUsernames() {
    usersRef.onValue.listen((event) {
      final dataSnapshot = event.snapshot;
      final userList = dataSnapshot.value as List<dynamic>;

      setState(() {
        usernames = List<String>.from(userList);
        startBattle(widget.username);
      });
    });
  }

void startBattle(String username) {
    if (usernames.length >= 2 && (usernames[0] == username || usernames[1] == username)) {
      List<String>_usernames = List.from(usernames);

      usersRef.remove();
      usernames.clear();

      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => VersusModeDisplay(
            usernames: _usernames,
            username: widget.username,
            questionDataFunction: widget.questionDataFunction, 
            serverId: 'Server: ${_usernames[0]} vs ${_usernames[0]}',
          ),
        ),
      );
    }
  }

  void addUser(String username) {
    if (!usernames.contains(username)) {
      usernames.add(username);
      usersRef.set(usernames);
    }
  }

  void removeUser(String username) {
    if (usernames.contains(username)) {
      usernames.remove(username);
      usersRef.set(usernames);
    }
  }

  @override
  Widget build(BuildContext context) {
    String username = widget.username;
    return WillPopScope(
      onWillPop: () async {
        bool canPop = true;
        removeUser(username);
        return canPop;
      },
    
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Data Screen'),
        ),
        body: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Usernames:',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
              ),
              const SizedBox(height: 8),
              ListView.builder(
                shrinkWrap: true,
                itemCount: usernames.length,
                itemBuilder: (context, index) {
                  //final user = usernames[index];
                  return Text(usernames[index]);
                },
              ),
              ElevatedButton(
                onPressed: () {
                  addUser(username);
                }, 
                child: const Text('join lobby')
                )
            ],
          ),
        ),
      ),
    );
  }
}

class Server {
  Server(int hp1, int hp2);
}