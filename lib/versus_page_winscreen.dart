import 'package:flutter/material.dart';

import 'main.dart';

class VersusModeWinScreen extends StatefulWidget {
  String winName;
  final Function(int) questionDataFunction;

  VersusModeWinScreen({
    Key? key,
    required this.winName, required this.questionDataFunction,
  }) : super(key: key);

  @override
  State<VersusModeWinScreen> createState() => VersusModeWinScreenPage();
}

class VersusModeWinScreenPage extends State<VersusModeWinScreen> {

  @override
  Widget build(BuildContext context) {
    return WillPopScope( 
      onWillPop: () async {
        return false;
      },
      child: Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Column(
              children: [
                Text(
                  '${widget.winName} wins!',
                  style: const TextStyle(fontSize: 28, fontWeight: FontWeight.bold),
                ),
                const Text(
                  '- and is the better programmer.',
                  style: TextStyle(fontSize: 20),
                ),
              ]
            ),
          ),
          const SizedBox(height: 16),
          ElevatedButton(
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const MyHomePage()),
              );
            },
            child: const Text('Return to homescreen'),
          ),
        ],
      ),
    )
    );
  }
}